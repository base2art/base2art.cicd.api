namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface ISecurityService
    {
        Task<IAccessData[]> GetAccessRecordsByUser(string username);
        Task<IRecordAccessData[]> GetAccessRecordsByUserAndObjectType(string username, Type objectType);
        
        Task<IAccessData[]> AddAccessRecord(string username, Type objectType, OperationType operationType);
        Task<IAccessData[]> AddAccessRecord(string username, Type objectType, OperationType operationType, Guid objectId);
        
        Task<IAccessData[]> DenyAccessRecord(string username, Type objectType, OperationType operationType);
        Task<IAccessData[]> DenyAccessRecord(string username, Type objectType, OperationType operationType, Guid objectId);
    }
}