﻿namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Threading.Tasks;

    public interface IVersionRepository
    {
        Task SetLastVersion(Guid projectId, Guid jobId, string branchIdentifier, string version);
        Task<string> GetLastVersion(Guid projectId, string branchIdentifier);
    }
}