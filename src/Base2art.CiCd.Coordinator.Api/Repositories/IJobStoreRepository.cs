namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IJobStoreRepository
    {
        Task SetState(Guid jobId, JobState state);

        Task SetBuildInfo(Guid jobId, Guid buildServer, Guid buildId);

        Task AddMessage(
            Guid buildId,
            JobPhase phase,
            string message,
            Dictionary<string, object> data);

        Task<IJobInfo> GetJobInfo(Guid jobId);

        Task<IJobMessage[]> GetJobMessages(Guid jobId);

        Task<IJobInfo[]> FindJobs(
            JobState? searchDataState,
            string searchDataBranchHash,
            string searchDataBranchName,
            Guid? searchDataProjectId,
            int pageSize,
            int pageIndex,
            JobSort sort);
        
        Task<IJobInfo[]> FindMostRecentJobsByProjectAndBranch(
            JobState? searchDataState,
            int pageSize,
            int pageIndex,
            JobSort sort);

        Task CreateJob(
            Guid jobId,
            Guid projectId,
            string projectName,
            string userName,
            Dictionary<Guid, SourceControlBranchData> newBranches);

        Task<IJobInfo[]> FindLatestJobs(SourceControlBranchData[] sourceControlBranchData);
    }
}