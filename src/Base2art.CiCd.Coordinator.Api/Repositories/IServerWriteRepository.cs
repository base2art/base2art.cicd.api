namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface IServerWriteRepository
    {
        Task<IServer> AddServer(ServerType pluginType, string name, string baseUrl, Guid? authenticationKeyId);
        Task<IServer> UpdateServer(Guid serverId, string name, string baseUrl, Guid? authenticationKeyId);
        Task DeleteServer(Guid serverId);
        Task<IServer> SetFeatures(Guid serverId, ServerFeature[] features);
        Task SetStatus(Guid serverId, bool enabled);
    }
}