namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IProjectRepository
    {
        Task<IProject[]> GetAll();

        Task<IProject[]> GetAllActive();

        Task<IProject> GetProjectById(Guid projectId);

        Task DeactivateProject(Guid projectId);

        Task CreateProject(
            Guid projectId,
            string name,
//            string[] features,
            VersioningStrategy versioningStrategy,
            Dictionary<string, string> versioningData);

        Task UpdateProject(
            Guid projectId,
            string name,
//            string[] features,
            VersioningStrategy versioningStrategy,
            Dictionary<string, string> versioningData);

        Task AddSourceControl(
            Guid projectId,
            Guid scSourceControlType,
            Dictionary<string, object> scSourceControlParameters,
            BuildInstructionData[] scInstructions);
        
        
        Task UpdateSourceControl(
            Guid projectId,
            Guid sourceControlId,
            Guid scSourceControlType,
            Dictionary<string, object> scSourceControlParameters,
            BuildInstructionData[] scInstructions);

        Task RemoveSourceControl(
            Guid projectId,
            Guid sourceControlId);

        Task AddFeature(
            Guid projectId,
            Guid featureId);

        Task RemoveFeature(
            Guid projectId,
            Guid featureId);

        Task SetFeatures(Guid projectId, Guid[] features);
    }
}