﻿namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface IBuildRepository
    {
        Task SetStatus(Guid buildId, BuildState buildState);

        Task<BuildState> GetStatus(Guid id);
    }
}