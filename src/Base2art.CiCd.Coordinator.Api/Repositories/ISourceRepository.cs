namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface ISourceRepository
    {
        Task<IBranchLogData[]> GetBranches(Guid sourceControlId);
        Task<IBranchLogData[]> GetBranches(Guid sourceControlId, DateTime olderThan);

        Task<IReadOnlyDictionary<Guid, IBranchLogData[]>> GetBranches(Guid[] sourceControlId);
        Task<IReadOnlyDictionary<Guid, IBranchLogData[]>> GetBranches(Guid[] sourceControlId, DateTime olderThan);

        Task<bool> SetBranches(
            Guid sourceControlId,
            SourceControlBranchData[] branches);

        Task SetCommits(
            Guid sourceControlId,
            string branchName,
            string commitHash,
            SourceControlBranchCommitLogData[] commits);

        Task<Guid[]> GetProjectsMissingBranches();
    }
}