namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface IServerReadRepository
    {
        Task<IServer[]> GetServers(ServerType? pluginType);

        Task<IServer> GetServer(Guid serverId);

        Task<IServer[]> FindByFeature(Guid featureId);

        Task<IServerFeature[]> AllKnownFeatures();
    }
}