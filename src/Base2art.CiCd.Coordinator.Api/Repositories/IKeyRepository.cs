namespace Base2art.CiCd.Coordinator.Repositories
{
    using System;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Models;

    public interface IKeyRepository
    {
        Task<IKey[]> GetAllKeys();
        Task<IKey[]> GetActiveKeys();
        Task<IKey[]> GetArchivedKeys();
        Task<IKey> GetKeyById(Guid keyId);

        Task<IKey> AddKey(string name, RSAParameters key);
        Task ArchiveKey(Guid keyId);
    }
}