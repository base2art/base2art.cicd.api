﻿namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Threading.Tasks;
    using Models;

    public interface IServerFeatureService
    {
        Task<IServerFeature[]> GetSupportedFeatures(Guid serverId);
    }
}