namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Models;
    using Repositories;

    public static class Servers
    {
        public static async Task<Guid> GetBuildServerIdByFeatures(
            this IServerReadRepository serverService,
            ServerType serverType,
            Guid[] projectRequiredFeatures)
        {
            var server = await GetBuildServerByFeatures(serverService, serverType, projectRequiredFeatures);
            return server?.Id ?? Guid.Empty;
        }

        public static async Task<IServer> GetBuildServerByFeatures(
            this IServerReadRepository serverService,
            ServerType serverType,
            Guid[] projectRequiredFeatures)
        {
            var requirements = new HashSet<Guid>(projectRequiredFeatures ?? new Guid[0]);

            var servers = await serverService.GetServers(serverType);
            var server = servers.Where(x => x != null)
                                .Where(x => x.Enabled)
                                .Where(x => requirements.IsSubsetOf(x.Features.Select(y => y.Id)))
                                .OrderBy(x => Guid.NewGuid())
                                .FirstOrDefault();

            return server;
        }

        public static async Task<IServer[]> GetBuildServersByFeatures(
            this IServerReadRepository serverService,
            ServerType? serverType,
            Guid[] projectRequiredFeatures)
        {
            var requirements = new HashSet<Guid>(projectRequiredFeatures ?? new Guid[0]);

            var servers = await serverService.GetServers(serverType);
            var server = servers.Where(x => x != null)
//                                .OrderBy(x => Guid.NewGuid())
                                .Where(x => requirements.IsSubsetOf(x.Features.Select(y => y.Id)));

            return server.ToArray();
        }

//        public static async Task<Dictionary<Guid, string[]>> GetFeaturesByServer(this IServerReadRepository serverService, ServerType serverType)
//        {
//            var servers = await serverService.GetServers(serverType);
//            return servers.ToDictionary(x => x.Id, x => x.Features.ToArray());
//        }
    }
}