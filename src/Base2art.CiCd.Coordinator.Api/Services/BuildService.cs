﻿namespace Poc.CiCd.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    using Repositories;

    public class BuildService : IBuildService
    {
        private readonly IBuildServerServiceClient client;
        private readonly IServerReadRepository serverService;
        private readonly IBuildRepository storage;

        public BuildService(
            IBuildRepository storage,
            IServerReadRepository serverService,
            IBuildServerServiceClient client)
        {
            this.storage = storage;
            this.serverService = serverService;
            this.client = client;
        }

        public async Task<IBuild> StartBuild(Guid buildServerId,
                                             string version,
                                             Dictionary<Guid, BuildInstructionData[]> buildInstructionLookup,
                                             Dictionary<Guid, byte[]> contentLookup,
                                             Dictionary<Guid, SourceControlBranchData> checkoutInfoLookup)
        {
            var build = new BuildModel();
            build.Id = Guid.NewGuid();
            build.ServerId = buildServerId;
            build.State = BuildState.Claimed;
            await this.storage.SetStatus(build.Id, build.State);

            var task = this.BuildSync(buildServerId, build.Id, version, buildInstructionLookup, contentLookup, checkoutInfoLookup);
            task.RunAway(ex => this.storage.SetStatus(build.Id, BuildState.CompletedFail));

            return build;
        }

        public async Task<IBuild> GetBuildStatus(Guid serverId, Guid id)
            => new BuildModel
               {
                   Id = id,
                   ServerId = serverId,
                   State = await this.storage.GetStatus(id)
               };

        public async Task<IReadOnlyDictionary<Guid, IEnumerable<IFileData>>> GetArtifacts(
            Guid serverId,
            Guid id,
            Guid[] sourceControlIds)
        {
            var server = await this.serverService.GetServer(serverId);

            return await this.client.GetArtifacts(server.Url, id, sourceControlIds);
        }

        public async Task RemoveBuild(Guid serverId, Guid id)
        {
            var server = await this.serverService.GetServer(serverId);
            await this.client.Remove(server.Url, id);
        }

        public async Task<IBuildLog> GetBuildLogs(Guid serverId, Guid buildId)
        {
            if (buildId == Guid.Empty || serverId == Guid.Empty)
            {
                return new BuildLogModel
                       {
                           Error = string.Empty,
                           Output = string.Empty
                       };
            }

            var server = await this.serverService.GetServer(serverId);
            if (server == null)
            {
                return new BuildLogModel
                       {
                           Error = $"Server Not found: {serverId}",
                           Output = $"Build Id: {buildId}"
                       };
            }

            var build = await this.client.GetBuild(server.Url, buildId);
            return new BuildLogModel
                   {
                       Output = build?.Output,
                       Error = build?.Error
                   };
        }

        private async Task BuildSync(
            Guid serverId,
            Guid id,
            string version,
            IDictionary<Guid, BuildInstructionData[]> buildInstructionLookup,
            IDictionary<Guid, byte[]> contentLookup,
            IDictionary<Guid, SourceControlBranchData> checkoutInfoLookup)
        {
            var server = await this.serverService.GetServer(serverId);
            var status = await this.client.Build(server.Url, id, version, buildInstructionLookup, contentLookup, checkoutInfoLookup);

            await this.storage.SetStatus(id, (BuildState) (int) status.State);
        }

        private class BuildModel : IBuild
        {
            public Guid Id { get; set; }
            public Guid ServerId { get; set; }
            public BuildState State { get; set; }
        }

        private class BuildLogModel : IBuildLog
        {
            public string Output { get; set; }
            public string Error { get; set; }
        }
    }
}