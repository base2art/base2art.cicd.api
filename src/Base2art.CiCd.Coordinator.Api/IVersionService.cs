namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IVersionService
    {
        Task<string> GetLastVersion(
            Guid projectId,
            Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup);

        Task<string> GetNextVersion(
            Guid projectId,
            Guid jobId,
            VersioningData versioningData,
            Dictionary<Guid, SourceControlBranchData> sourceControlInfoLookup);
    }
}