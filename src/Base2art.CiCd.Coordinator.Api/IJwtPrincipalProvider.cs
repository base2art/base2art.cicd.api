namespace Base2art.CiCd.Coordinator
{
    using System.Security.Claims;

    public interface IJwtPrincipalProvider
    {
        (string token, ClaimsPrincipal principal) Create();
    }
}