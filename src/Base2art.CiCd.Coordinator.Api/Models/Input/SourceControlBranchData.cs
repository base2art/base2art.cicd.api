namespace Base2art.CiCd.Coordinator.Models
{
    public class SourceControlBranchData
    {
        public string Name { get; set; }
        public string Hash { get; set; }

        public override string ToString() => $"{nameof(this.Name)}: {this.Name}, {nameof(this.Hash)}: {this.Hash}";
    }
}