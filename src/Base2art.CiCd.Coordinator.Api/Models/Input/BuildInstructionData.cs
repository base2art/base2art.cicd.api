namespace Base2art.CiCd.Coordinator.Models
{
    public class BuildInstructionData
    {
        public string[] ExecutableArguments { get; set; }
        public string ExecutableFile { get; set; }
    }
}