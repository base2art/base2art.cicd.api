namespace Base2art.CiCd.Coordinator.Models
{
    using System.Collections.Generic;

    public class VersioningData
    {
        public VersioningStrategy Strategy { get; set; }
        public Dictionary<string, string> DefaultData { get; set; }

        public override string ToString() => $"{nameof(this.Strategy)}: {this.Strategy}";
    }
}