namespace Base2art.CiCd.Coordinator.Models
{
    using System;
    using System.Collections.Generic;

    public class ServerFeature
    {
        public string SimpleName { get; set; }
        public string FullName { get; set; }
        public Dictionary<string, object> DefaultData { get; set; }
        public Guid Id { get; set; }
    }
}