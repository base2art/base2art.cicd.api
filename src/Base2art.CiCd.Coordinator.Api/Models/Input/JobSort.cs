﻿namespace Base2art.CiCd.Coordinator.Models
{
    public enum JobSort
    {
        DateCreatedAscending,
        DateCreatedDescending
    }
}