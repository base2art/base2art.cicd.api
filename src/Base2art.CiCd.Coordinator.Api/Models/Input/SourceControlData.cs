namespace Base2art.CiCd.Coordinator.Models
{
    using System;
    using System.Collections.Generic;

    public class SourceControlData
    {
        public Guid SourceControlType { get; set; }
        public Dictionary<string, object> SourceControlParameters { get; set; }
        public BuildInstructionData[] Instructions { get; set; }
    }
}