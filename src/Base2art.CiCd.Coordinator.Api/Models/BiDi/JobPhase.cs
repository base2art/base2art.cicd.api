namespace Base2art.CiCd.Coordinator.Models
{
    public enum JobPhase
    {
        Creation = 1,
        Checkout = 4,
        Build = 8,
        Publish = 12,
        Completion = 16,
        SystemCleanup = 32,
        Notification = 64,
    }
}