namespace Base2art.CiCd.Coordinator.Models
{
    public enum ServerType
    {
        SourceControl = 1,
        Compilation = 2,
        Publish = 3,
        Notification = 4,
    }
}