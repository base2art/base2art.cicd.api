﻿namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    [Flags]
    public enum JobState
    {
        Unknown = 0,
        Pending = 1,
        Claimed = 2,
        Working = 4,
        CompletedSuccess = 8,
        CompletedFail = 16,

        RunningLong = 32
    }
}