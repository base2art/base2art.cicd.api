﻿namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    public interface IBuild
    {
        Guid Id { get; }

        Guid ServerId { get; }

        BuildState State { get; }
    }
}