namespace Base2art.CiCd.Coordinator.Models
{
    public interface IFileData
    {
        string RelativeName { get; }
        string Name { get; }
        byte[] Content { get; }
    }
}