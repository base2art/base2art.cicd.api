namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    public interface ICommitLogData
    {
        string Id { get; }
        DateTime When { get; }
        string Author { get; }
        string Message { get; }
        string[] FilesAdded { get; }
        string[] FilesRemoved { get; }
        string[] FilesChanged { get; }
    }
}