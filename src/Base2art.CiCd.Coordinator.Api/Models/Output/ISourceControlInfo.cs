namespace Base2art.CiCd.Coordinator.Models
{
    using System;
    using System.Collections.Generic;

    public interface ISourceControlInfo
    {
        Guid Id { get; }
        Guid SourceControlType { get; }
        IReadOnlyDictionary<string, string> SourceControlData { get; }
        IBuildInstruction[] BuildInstructions { get; }
    }
}