namespace Base2art.CiCd.Coordinator.Models
{
    public enum PermissionMode
    {
        None,
        Partial,
        Full
    }
}