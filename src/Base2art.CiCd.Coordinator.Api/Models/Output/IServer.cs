namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    public interface IServer
    {
        string Name { get; }
        Guid Id { get; }
        bool Enabled { get; }
        IServerFeature[] Features { get; }
        Uri Url { get; }
        Guid? AuthenticationKeyId { get; }

        ServerType ServerType { get; }
        DateTime Modified { get; }
    }
}