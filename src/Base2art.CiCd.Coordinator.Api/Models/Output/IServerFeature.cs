namespace Base2art.CiCd.Coordinator.Models
{
    using System;
    using System.Collections.Generic;

    public interface IServerFeature
    {
        string SimpleName { get; }
        string FullName { get; }
        IReadOnlyDictionary<string, object> DefaultData { get; }
        Guid Id { get; }
    }
}