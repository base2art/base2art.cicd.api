﻿namespace Base2art.CiCd.Coordinator.Models
{
    public interface IBuildLog
    {
        string Output { get; }
        string Error { get; }
    }
}