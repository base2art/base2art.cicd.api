namespace Base2art.CiCd.Coordinator.Models
{
    public interface IBuildInstruction
    {
        string[] ExecutableArguments { get; }
        string ExecutableFile { get; }
    }
}