namespace Base2art.CiCd.Coordinator.Models
{
    public interface IBranchLogData
    {
        string Name { get; }
        string Hash { get; }
        ICommitLogData[] CommitLogs { get; }
    }
}