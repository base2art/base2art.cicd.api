namespace Base2art.CiCd.Coordinator.Models
{
    public enum OperationType
    {
        List,
        View,
        Add,
        Edit,
        Delete
    }
}