namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    public interface IRecordAccessData
    {
        Guid Id { get; }
        Type ObjectType { get; }
        Guid ObjectId { get; }
        OperationType OperationType { get; }
    }
}