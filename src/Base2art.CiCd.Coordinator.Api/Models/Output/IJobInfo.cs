namespace Base2art.CiCd.Coordinator.Models
{
    using System;
    using System.Collections.Generic;

    public interface IJobInfo
    {
        Guid Id { get; }

        Guid ProjectId { get; }

        JobState State { get; }

        Guid? BuildServer { get; }

        Guid? Build { get; }

        IReadOnlyDictionary<Guid, IBranchLogData> SourceCode { get; }

        DateTime DateCreated { get; }

        DateTime? DateFinished { get; }

        string OriginatingUser { get; }
        
        string ProjectName { get; }
        
        string Version { get; }
    }
}