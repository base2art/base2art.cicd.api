namespace Base2art.CiCd.Coordinator.Models
{
    using System.Collections.Generic;

    public interface IVersioning
    {
        VersioningStrategy Strategy { get; }
        IReadOnlyDictionary<string, string> DefaultData { get; }
    }
}