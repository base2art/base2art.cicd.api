namespace Base2art.CiCd.Coordinator.Models
{
    using System.Collections.Generic;

    public interface IJobMessage
    {
        string Phase { get; }
        string Message { get; }
        IReadOnlyDictionary<string, object> Data { get; }
    }
}