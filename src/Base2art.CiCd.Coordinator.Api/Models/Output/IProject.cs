namespace Base2art.CiCd.Coordinator.Models
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography.X509Certificates;

    public interface IProject
    {
        IEnumerable<ISourceControlInfo> SourceControl { get; }
        Guid Id { get; }
        string Name { get; }

        Guid[] RequiredFeatures { get; }
        bool IsActive { get; }
        DateTime Created { get; }
        IVersioning Versioning { get; }
    }
}