namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    public interface IKey
    {
        Guid Id { get; }
        
        string Name { get; }

        bool IsActive { get; }

        byte[] D { get; }
        byte[] DP { get; }
        byte[] DQ { get; }
        byte[] Exponent { get; }
        byte[] InverseQ { get; }
        byte[] Modulus { get; }
        byte[] P { get; }
        byte[] Q { get; }
    }
}