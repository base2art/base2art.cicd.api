namespace Base2art.CiCd.Coordinator.Models
{
    using System;

    public interface IAccessData
    {
        Guid Id { get; }
        Type ObjectType { get; }
        PermissionMode PermissionMode { get; }
        OperationType OperationType { get; }
    }
}