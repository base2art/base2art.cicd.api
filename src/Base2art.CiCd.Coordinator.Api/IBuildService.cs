﻿namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IBuildService
    {
        Task<IBuild> StartBuild(Guid buildServerId,
                                string version,
                                Dictionary<Guid, BuildInstructionData[]> buildInstructionLookup,
                                Dictionary<Guid, byte[]> contentLookup,
                                Dictionary<Guid, SourceControlBranchData> checkoutInfoLookup);

        Task<IBuild> GetBuildStatus(Guid serverId, Guid id);

        Task<IReadOnlyDictionary<Guid, IEnumerable<IFileData>>> GetArtifacts(
            Guid serverId,
            Guid id,
            Guid[] sourceControlIds);

        Task RemoveBuild(Guid serverId, Guid id);

        Task<IBuildLog> GetBuildLogs(Guid serverId, Guid buildId);
    }
}