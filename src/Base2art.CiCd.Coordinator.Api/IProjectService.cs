﻿namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IProjectService
    {
        Task<IProject[]> GetProjects(bool includeInactive);

        Task<IProject> GetProjectById(Guid projectId);

        Task<IProject> DeactivateProject(Guid projectId);

        Task<IProject> SaveProject(
            Guid projectId,
            string name,
            VersioningStrategy? versioningStrategy,
            Dictionary<string, string> dictionary);

        Task AddSourceControl(
            Guid projectId,
            SourceControlData sourceControlData);
        
        
        Task UpdateSourceControl(
            Guid projectId,
            Guid sourceControlId,
            SourceControlData sourceControlData);

        Task RemoveSourceControl(
            Guid projectId,
            Guid sourceControlId);

        Task AddFeature(Guid projectId, Guid featureId);
        
        Task SetProjectFeatures(Guid projectId, Guid[] features);

        Task RemoveFeature(Guid projectId, Guid featureId);
    }
}