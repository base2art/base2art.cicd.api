namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Threading.Tasks;

    public static class TaskTrapper
    {
        public static async void RunAway(this Task task, Func<Exception, Task> onError)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                try
                {
                    if (onError != null)
                    {
                        await onError(ex);
                    }
                }
                catch
                {
                }
            }
        }

        public static void RunAway(this Task task)
        {
            task.RunAway(null);
        }

        public static void TryRun<T>(int max, Action action)
            where T : Exception
        {
            TryRun<T>(0, max, action, false);
        }

        public static void TryRunSafe<T>(int max, Action action)
            where T : Exception
        {
            TryRun<T>(0, max, action, true);
        }

        private static void TryRun<T>(int i, int max, Action action, bool suppress)
            where T : Exception
        {
            try
            {
                action();
            }
            catch (T)
            {
                if (i > max)
                {
                    if (suppress)
                    {
                        return;
                    }

                    throw;
                }

                TryRun<T>(i + 1, max, action, suppress);
            }
        }
    }
}