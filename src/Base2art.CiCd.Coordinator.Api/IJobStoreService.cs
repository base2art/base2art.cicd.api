﻿namespace Base2art.CiCd.Coordinator
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;

    public interface IJobStoreService
    {
        Task<IJobInfo> SetState(Guid jobId, JobState state);

        Task<IJobInfo> SetBuildInfo(Guid jobId, Guid buildServer, Guid buildId);

        Task AddMessage(
            Guid buildId,
            string projectName,
            JobPhase phase,
            string message,
            Dictionary<string, object> data);

        Task<IJobInfo> GetJobInfo(Guid jobId);

        Task<IJobMessage[]> GetJobMessages(Guid jobId);

        Task<IJobInfo[]> FindJobs(
            JobState? searchDataState,
            string searchDataBranchHash,
            string searchDataBranchName,
            Guid? searchDataProjectId,
            int pageSize,
            int pageIndex,
            JobSort sort);
        
        Task<IJobInfo[]> FindMostRecentJobsByProjectAndBranch(
            JobState? searchDataState,
            int pageSize,
            int pageIndex,
            JobSort sort);

        Task<IJobInfo[]> FindLatestJobs(SourceControlBranchData[] branchData);

        Task<IJobInfo> CreateJob(
            Guid projectId,
            string projectName,
            string userName,
            Dictionary<Guid, SourceControlBranchData> newBranches);
    }
}