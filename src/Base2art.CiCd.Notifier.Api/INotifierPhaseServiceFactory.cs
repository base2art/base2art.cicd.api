namespace Base2art.CiCd.Notifier
{
    using Servers;

    public interface INotifierPhaseServiceFactory : IExtensionServiceFactory<INotifierPhaseService>
    {
    }
}