namespace Base2art.CiCd.Notifier
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Resources;
    using Servers;

    public interface INotifierPhaseService : ISupportedFeatureService
    {
        Task<ProjectNotification> StartNotification(Guid featureId, Notification eventDetails, ClaimsPrincipal principal);
        
        Task<ProjectNotification> GetNotificationStatus(Guid notificationId, ClaimsPrincipal principal);
    }

}