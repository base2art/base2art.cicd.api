namespace Base2art.CiCd.Notifier.Resources
{
    using System;

    public class Project
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}