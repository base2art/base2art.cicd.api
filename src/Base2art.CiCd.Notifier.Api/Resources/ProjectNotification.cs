namespace Base2art.CiCd.Notifier.Resources
{
    using System;

    public class ProjectNotification
    {
        public Guid Id { get; set; }
        
        public NotificationPhaseState State { get; set; }

        public string Output { get; set; }
        
        public string Error { get; set; }

        public DateTime? CompletedAt { get; set; }
    }
}