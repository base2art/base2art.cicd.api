namespace Base2art.CiCd.Notifier.Resources
{
    public enum JobPhaseState
    {
        Unknown = 0,
        Pending = 1,
        Claimed = 2,
        Working = 3,
        CompletedSuccess = 4,
        CompletedFail = 5
    }
}