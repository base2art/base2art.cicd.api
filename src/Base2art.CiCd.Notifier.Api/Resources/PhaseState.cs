namespace Base2art.CiCd.Notifier.Resources
{
    using System;

    public class PhaseState
    {
        public string Name { get; set; }
        public TimeSpan? Duration { get; set; }
        public bool? SuccessfullyCompleted { get; set; }
    }
}