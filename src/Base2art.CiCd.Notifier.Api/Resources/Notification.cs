namespace Base2art.CiCd.Notifier.Resources
{
    using System;

    public class Notification
    {
        public Guid JobId { get; set; }

        public Project Project { get; set; }

        public BranchData[] Branches { get; set; }
        public FileData[] Artifacts { get; set; }
        public PublishDetail[] PublishDetails { get; set; }

        public PhaseState[] Phases { get; set; }

        public TimeSpan? Duration { get; set; }

        public bool? SuccessfullyCompleted { get; set; }
    }
}