namespace Base2art.CiCd.Notifier.Resources
{
    using System;

    public class BranchData
    {
        public string Id { get; set; }
        public Guid TypeId { get; set; }
        public string Name { get; set; }
        public string Hash { get; set; }

        public override string ToString() => $"{nameof(this.Name)}: {this.Name}, {nameof(this.Hash)}: {this.Hash}";
    }
}