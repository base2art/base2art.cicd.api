namespace Base2art.CiCd.Notifier.Resources
{
    using System;

    public class PublishDetail
    {
        private string EventId { get; set; }
        
        public Guid HandlerId { get; set; }
        
        public string HandlerName { get; set; }
        
        public FileData[] FileData { get; set; }
    }
}