namespace Base2art.CiCd.Notifier.Resources
{
    public class FileData
    {
        public string Name { get; set; }
        public string RelativeName { get; set; }
    }
}