namespace Base2art.CiCd.Builder
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Resources;
    using Servers;

    public interface IBuildPhaseService : ISupportedFeatureService
    {
        Task<ProjectBuild> StartBuild(Guid buildId, string version, List<ProjectBuildContent> buildData, ClaimsPrincipal principal);

        Task<ProjectBuild> GetBuild(Guid buildId, ClaimsPrincipal principal);

        Task Remove(Guid buildId, ClaimsPrincipal principal);

        Task<Dictionary<Guid, Artifact[]>> GetArtifacts(Guid buildId, ClaimsPrincipal principal);
    }
}