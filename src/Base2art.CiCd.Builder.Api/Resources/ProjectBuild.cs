namespace Base2art.CiCd.Builder.Resources
{
    using System;

    public class ProjectBuild
    {
        public Guid BuildId { get; set; }

        public BuildPhaseState State { get; set; }

        public DateTime? CompletedAt { get; set; }

        public string Output { get; set; }

        public string Error { get; set; }

//        public List<ProjectBuildContent> Content { get; set; }
    }
}

/*
 *
        public string PowerShellScript { get; set; }

        public string SourceControlType { get; set; }

        public object SourceControlData { get; set; }
 * 
 */