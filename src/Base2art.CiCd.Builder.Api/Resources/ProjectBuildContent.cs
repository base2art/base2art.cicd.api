namespace Base2art.CiCd.Builder.Resources
{
    using System;

    public class ProjectBuildContent
    {
        public Guid SourceControlId { get; set; }
        public string BranchName { get; set; }
        public string BranchHash { get; set; }
        public byte[] Files { get; set; }
        public BuildInstruction[] BuildInstructions { get; set; }
    }
}