namespace Base2art.CiCd.Builder.Resources
{
    public enum BuildPhaseState
    {
        Unknown = 0,
        Pending = 1,
        Claimed = 2,
        Working = 3,
        CompletedSuccess = 4,
        CompletedFail = 5
    }
}