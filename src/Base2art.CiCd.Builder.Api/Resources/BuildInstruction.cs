namespace Base2art.CiCd.Builder.Resources
{
    public class BuildInstruction
    {
        public string[] Arguments { get; set; }
        public string Executable { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}