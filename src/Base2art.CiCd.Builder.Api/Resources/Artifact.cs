namespace Base2art.CiCd.Builder.Resources
{
    public class Artifact
    {
        public string RelativeName { get; set; }
        public string Name { get; set; }
        public byte[] Content { get; set; }
    }
}