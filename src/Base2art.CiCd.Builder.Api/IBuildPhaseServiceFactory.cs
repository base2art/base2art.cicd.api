namespace Base2art.CiCd.Builder
{
    using System;
    using System.Threading.Tasks;
    using Servers;

    public interface IBuildPhaseServiceFactory : IExtensionServiceFactory<IBuildPhaseService>
    {
    }
}