namespace Base2art.CiCd.SourceControl.Resources
{
    public class IsolatedBranchLogData
    {
        public string Name { get; set; }
        public string Hash { get; set; }

        public IsolatedCheckoutLogData[] Logs { get; set; }
    }
}