namespace Base2art.CiCd.SourceControl.Resources
{
    using System;

    public class IsolatedSourceData
    {
        public byte[] Content { get; set; }
        public Guid[] RequiredFeatureIds { get; set; }
    }
}