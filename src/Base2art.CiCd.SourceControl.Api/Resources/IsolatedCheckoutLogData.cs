namespace Base2art.CiCd.SourceControl.Resources
{
    using System;

    public class IsolatedCheckoutLogData
    {
        public string Id { get; set; }
        public DateTime When { get; set; }
        public string Author { get; set; }
        public string Message { get; set; }
        public string[] FilesAdded { get; set; }
        public string[] FilesRemoved { get; set; }
        public string[] FilesChanged { get; set; }
    }
}