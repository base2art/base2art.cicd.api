namespace Base2art.CiCd.SourceControl.Resources
{
    using System;
    using System.Collections.Generic;

    public class IsolatedSourceControlType
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public Dictionary<string, object> DefaultData { get; set; }
    }
}