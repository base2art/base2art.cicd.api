namespace Base2art.CiCd.SourceControl.Resources
{
    public class IsolatedBranchData
    {
        public string Name { get; set; }
        public string Hash { get; set; }
    }
}