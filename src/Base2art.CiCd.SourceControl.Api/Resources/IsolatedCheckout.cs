namespace Base2art.CiCd.SourceControl.Resources
{
    using System;

    public class IsolatedCheckout
    {
        public Guid Id { get; set; }

        public IsolatedCheckoutState State { get; set; }
    }
}