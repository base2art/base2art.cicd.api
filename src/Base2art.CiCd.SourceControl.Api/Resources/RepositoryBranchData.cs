namespace Base2art.CiCd.SourceControl.Resources
{
    public class RepositoryBranchData<T>
    {
        // GitRepositoryData
        public T Repository { get; set; }
        public IsolatedBranchData Branch { get; set; }
    }
}