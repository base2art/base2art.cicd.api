namespace Base2art.CiCd.SourceControl
{
    using System;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Resources;
    using Servers;

    public interface ICheckoutPhaseService : ISupportedFeatureService
    {
        Task<IsolatedBranchData[]> GetBranches(Dictionary<string, string> projectSourceControlData, ClaimsPrincipal principal);

        Task<IsolatedBranchLogData> GetLogs(RepositoryBranchData<Dictionary<string, string>> projectSourceControlData, ClaimsPrincipal principal);

        Task<IsolatedCheckout> StartCheckout(Dictionary<string, string> projectSourceControlData, string branchOrTag, ClaimsPrincipal principal);

        Task<IsolatedCheckout> GetCheckoutStatus(Guid checkoutId, ClaimsPrincipal principal);

        [Obsolete("Please use the metadata method", false)]
        Task<byte[]> GetSource(Guid checkoutId, ClaimsPrincipal principal);

        Task<IsolatedSourceData> GetSourceWithMetaData(Guid checkoutId, ClaimsPrincipal principal);

        Task RemoveSource(Guid checkoutId, ClaimsPrincipal principal);
    }

    public interface ICheckoutPhaseService<TRepositoryData, in TCheckoutData> : ICheckoutPhaseService, ISupportedFeatureService
    {
        Task<IsolatedBranchData[]> GetBranches(TRepositoryData projectSourceControlData, ClaimsPrincipal principal);

        Task<IsolatedBranchLogData> GetLogs(RepositoryBranchData<TRepositoryData> data, ClaimsPrincipal principal);

        Task<IsolatedCheckout> StartCheckout(TCheckoutData projectSourceControlData, ClaimsPrincipal principal);
    }
}