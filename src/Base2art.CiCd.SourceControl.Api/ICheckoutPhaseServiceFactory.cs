namespace Base2art.CiCd.SourceControl
{
    using System;
    using System.Threading.Tasks;
    using Servers;

    public interface ICheckoutPhaseServiceFactory : IExtensionServiceFactory<ICheckoutPhaseService>
    {
    }
}