namespace Base2art.CiCd.Publisher.Resources
{
    public class FileData
    {
        public string Name { get; set; }
        public string RelativeName { get; set; }
        public byte[] Content { get; set; }
    }
}