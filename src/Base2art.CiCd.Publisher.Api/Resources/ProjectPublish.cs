namespace Base2art.CiCd.Publisher.Resources
{
    using System;

    public class ProjectPublish
    {
        public Guid Id { get; set; }
        public PublishPhaseState State { get; set; }
        
        public string Output { get; set; }
        public string Error { get; set; }
        
        public DateTime? CompletedAt { get; set; }
    }
}