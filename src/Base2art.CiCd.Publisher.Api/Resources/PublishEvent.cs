namespace Base2art.CiCd.Publisher.Resources
{
    using System;

    public class PublishEvent
    {
        public Guid JobId { get; set; }

        public Project Project { get; set; }
        public BranchData[] Branches { get; set; }
        public FileData[] Artifacts { get; set; }
    }
}