namespace Base2art.CiCd.Publisher
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Resources;
    using Servers;

    public interface IPublishPhaseService : ISupportedFeatureService
    {
        Task<ProjectPublish> StartPublish(Guid featureId, PublishEvent eventDetails, ClaimsPrincipal principal);
        
        Task<ProjectPublish> GetPublishStatus(Guid publishEventId, ClaimsPrincipal principal);
    }
}