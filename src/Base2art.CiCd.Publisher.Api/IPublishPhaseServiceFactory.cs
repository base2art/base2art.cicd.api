namespace Base2art.CiCd.Publisher
{
    using Servers;

    public interface IPublishPhaseServiceFactory : IExtensionServiceFactory<IPublishPhaseService>
    {
    }
}