namespace Base2art.CiCd.Servers
{
    using System;
    using System.Threading.Tasks;

    public interface IExtensionServiceFactory<T>
    {
        Task<IExtensionServiceClient<T>> CreateClient(Guid projectTypeId);
        Task<T> CreateClient(string baseUrl);
    }
}