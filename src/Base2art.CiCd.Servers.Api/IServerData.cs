namespace Base2art.CiCd.Servers
{
    using System;

    public interface IServerData
    {
        Guid Id { get; }
        string Name { get; }
        Uri Url { get; }
        Guid? AuthenticationKeyId { get; }
    }
}