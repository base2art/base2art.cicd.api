namespace Base2art.CiCd.Servers
{
    using System;

    public class SupportedFeature
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public object DefaultData { get; set; }
    }
}