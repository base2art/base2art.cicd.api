namespace Base2art.CiCd.Servers
{
    using System.Security.Claims;
    using System.Threading.Tasks;

    public interface ISupportedFeatureService
    {
        Task<SupportedFeature[]> GetSupportedFeatures(ClaimsPrincipal principal);
    }
}