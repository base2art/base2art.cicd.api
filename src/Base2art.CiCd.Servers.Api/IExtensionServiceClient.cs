namespace Base2art.CiCd.Servers
{
    public interface IExtensionServiceClient<out T>
    {
        IServerData[] AlternateServers { get; }
        IServerData CurrentServer { get; }
        T Service { get; }
    }
}